package com.assignment.todolist.helper;

public class ConvertUtil {
	public static String toString(Object obj){
		String str = "";
		try{
			if(obj != null){
				str = obj.toString();
			}
		}catch(Exception e){}
		return str;
	}
	
	public static int toInt(Object obj){
		int num = 0;
		try{
			if(obj != null){
				num = Integer.parseInt(toString(obj));
			}
		}catch(Exception e){}
		return num;
	}
	
	public static double toDouble(Object obj){
		double num = 0;
		try{
			if(obj != null){
				num = Double.parseDouble(toString(obj));
			}
		}catch(Exception e){}
		return num;
	}
}
