package com.assignment.todolist.consts;

public class Constants {
	public static class Status {
		public static int PENDING = 0;
		public static int DONE = 1;
	}
	
	public static class Active{
		public static byte NO = 0;
		public static byte YES = 1;		
	}
}

