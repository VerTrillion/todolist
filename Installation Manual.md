# Installation Manual #

## Required Software
-----------------------

1. Microsoft SQL Server

2. SQL Server Management Studio

3. Eclipse

4. JBoss EAP 7.0


## Configure Microsoft SQL Server
-----------------------

1. create Database **_TodoList_**
2. create Table **_Task_** by using following script in SSMS

        :::sql
        USE [TodoList]  
        GO  
        SET ANSI_NULLS ON  
        GO  
        SET QUOTED_IDENTIFIER ON  
        GO  
        CREATE TABLE [dbo].[Task](  
          [Id] [int] IDENTITY(1,1) NOT NULL,  
          [Subject] [nvarchar](50) NOT NULL,  
          [Description] [nvarchar](max) NULL,  
          [StatusId] [int] NOT NULL,  
          [Active] [bit] NOT NULL  
        ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]  
        GO  
        ALTER TABLE [dbo].[Task] ADD  CONSTRAINT [DF_Task_Active]  DEFAULT ((1)) FOR [Active]  
        GO  

3. create Table **_Status_** by using following script in SSMS

        :::sql
        USE [TodoList]  
        GO  
        SET ANSI_NULLS ON  
        GO  
        SET QUOTED_IDENTIFIER ON  
        GO  
        CREATE TABLE [dbo].[Status](  
          [Id] [int] IDENTITY(0,1) NOT NULL,  
          [StatusName] [nvarchar](50) NOT NULL  
        ) ON [PRIMARY]  
        GO  

4. add initial data into Table **_Status_** by using following script in SSMS

        :::sql
        INSERT INTO [dbo].[Status]([StatusName])  
        VALUES ('Pending');  
        INSERT INTO [dbo].[Status]([StatusName])  
        VALUES ('Done');  

## Configure Eclipse
-----------------------

1. Create New Workspcae

2. New JBoss EAP 7.0 Server

3. Check out Source Code

4. Edit Class **_com.assignment.todolist.datasource.ConnectionSupport_**

        :::java
        public void connect(){  
            try{  
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");  
                conn =  DriverManager.getConnection("jdbc:sqlserver://[HOST]\\[INSTANCE_NAME]:[PORT];databaseName=TodoList;user=[USER_NAME];password=[PASSWORD]");  
            }catch(Exception e){  
                logger.error("connect error:",e);  
            }  
        }  

5. Edit log path in **_/TodoListService/resource/log4j2.properties_**

6. Add TodoListServiceEAR into Server

7. Start Server

## Deploy manually
-----------------------

1. Export EAR from Eclipse

2. Access Jboss EAP Admin console

3. Deploy exported EAR