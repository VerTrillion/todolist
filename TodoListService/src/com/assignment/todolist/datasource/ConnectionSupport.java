package com.assignment.todolist.datasource;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class ConnectionSupport {
	public Connection conn;
	public String sql;
	public Statement stm;
	public PreparedStatement pstm;
	public CallableStatement cstm;
	public ResultSet rs;
	
	private static final Logger logger = LogManager.getLogger(ConnectionSupport.class);
	
	public void connect(){
		try{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			conn =  DriverManager.getConnection("jdbc:sqlserver://localhost\\SQLEXPRESS:1433;databaseName=TodoList;user=sa;password=P@ssw12rd");			
		}catch(Exception e){
			logger.error("connect error:",e);
		}
	}
	
	public void flush(){
		
		partialFlush();
		
		if(conn != null){
			try{
				if(!conn.isClosed()){
					conn.close();
				}
			}catch(Exception e){}			
		}
	}
	
	public void partialFlush(){
		if(stm != null){
			try{
				if(!stm.isClosed()){
					stm.close();
				}
			}catch(Exception e){}			
		}
		
		if(pstm != null){
			try{
				if(!pstm.isClosed()){
					pstm.close();
				}
			}catch(Exception e){}			
		}
		
		if(cstm != null){
			try{
				if(!cstm.isClosed()){
					cstm.close();
				}
			}catch(Exception e){}			
		}
		
		if(rs != null){
			try{
				if(!rs.isClosed()){
					rs.close();
				}
			}catch(Exception e){}			
		}
	}
	
}
