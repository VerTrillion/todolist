package com.assignment.todolist.dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.assignment.todolist.datasource.ConnectionSupport;
import com.assignment.todolist.model.Task;
import com.assignment.todolist.consts.Constants;

public class TaskDAO extends ConnectionSupport {

private static final Logger logger = LogManager.getLogger(TaskDAO.class);
	
	public Task get(int id) throws Exception{
		Task task = null;
		try{
			logger.debug("get >> id:"+id);
			
			super.connect();
			
			if(conn != null){
				StringBuffer buffer = new StringBuffer("");
				buffer.append(" select * ");
				buffer.append(" from Task t ");
				buffer.append(" where t.Id = ? ");
				buffer.append(" and t.Active = ? ");
				sql = buffer.toString();
				
				pstm = conn.prepareStatement(sql);
				
				byte i = 1;
				pstm.setInt(i++, id);
				pstm.setByte(i++, Constants.Active.YES);
				
				rs = pstm.executeQuery();
				
				if(rs.next()){
					task = new Task();
					setData(rs, task);
				}
			}
		}catch(Exception e){
			logger.error("get error:",e);
			throw e;
		}finally{
			super.flush();
		}
		return task;
	}
	
	public List<Task> getAll() throws Exception{
		List<Task> taskList = null;
		try{
			logger.debug("getAll");
			
			super.connect();
			
			if(conn != null){
				StringBuffer buffer = new StringBuffer("");
				buffer.append(" select * ");
				buffer.append(" from Task t ");
				buffer.append(" where t.Active = ? ");
				sql = buffer.toString();
				
				pstm = conn.prepareStatement(sql);
				
				byte i = 1;
				pstm.setByte(i++, Constants.Active.YES);
				
				rs = pstm.executeQuery();
				
				while(rs.next()){
					if(taskList == null){
						taskList = new ArrayList<Task>();
					}
					
					Task task = new Task();
					setData(rs, task);
					
					taskList.add(task);
				}
			}
		}catch(Exception e){
			logger.error("getAll error:",e);
			throw e;
		}finally{
			super.flush();
		}
		return taskList;
	}
	
	private void setData(ResultSet rs, Task task) throws Exception{
		try{
			task.setId(rs.getInt("Id"));
			task.setSubject(rs.getString("Subject"));
			task.setDesc(rs.getString("Description"));
			task.setStatusId(rs.getInt("StatusId"));
		}catch(Exception e){
			logger.error("setData error:",e);
			throw e;
		}
	}
	
	public int add(Task task) throws Exception{
		int id = 0;
		try{
			logger.debug("add >> task:"+task.getSubject());
			
			super.connect();
			
			if(conn != null){
				StringBuffer buffer = new StringBuffer("");
				buffer.append(" insert into Task(Subject, Description, StatusId, Active) ");
				buffer.append(" output Inserted.Id ");
				buffer.append(" values (?, ?, ?, ?) ");				
				sql = buffer.toString();
				
				pstm = conn.prepareStatement(sql);
				
				byte i = 1;
				pstm.setString(i++, task.getSubject());
				pstm.setString(i++, task.getDesc());
				pstm.setInt(i++, Constants.Status.PENDING);
				pstm.setByte(i++, Constants.Active.YES);
				
				rs = pstm.executeQuery();
				
				if(rs.next()){
					id = rs.getInt("Id");
				}
			}
		}catch(Exception e){
			logger.error("add error:",e);
			throw e;
		}finally{
			super.flush();
		}
		return id;
	}
	
	public int edit(Task task) throws Exception{
		int count = 0;
		try{
			logger.debug("edit >> task:"+task.getSubject());
			
			super.connect();
			
			if(conn != null){
				StringBuffer buffer = new StringBuffer("");
				buffer.append(" update Task ");
				buffer.append(" set ");
				buffer.append("  Subject = ?, ");
				buffer.append("  Description = ? ");
				buffer.append(" where id = ? ");
				
				sql = buffer.toString();
				
				pstm = conn.prepareStatement(sql);
				
				byte i = 1;
				pstm.setString(i++, task.getSubject());
				pstm.setString(i++, task.getDesc());
				pstm.setInt(i++, task.getId());
				
				count = pstm.executeUpdate();				
			}
		}catch(Exception e){
			logger.error("edit error:",e);
			throw e;
		}finally{
			super.flush();
		}
		return count;
	}
	
	public int setStatus(int id, int statusId) throws Exception{
		int count = 0;
		try{
			logger.debug("setStatus >> id:"+id+", statusid:"+statusId);
			
			super.connect();
			
			if(conn != null){
				StringBuffer buffer = new StringBuffer("");
				buffer.append(" update Task ");
				buffer.append("  set statusId = ? ");
				buffer.append(" where id = ? ");
				sql = buffer.toString();
				
				pstm = conn.prepareStatement(sql);
				
				byte i = 1;
				pstm.setInt(i++, statusId);
				pstm.setInt(i++, id);
				
				count = pstm.executeUpdate();
			}
		}catch(Exception e){
			logger.error("setStatus error:",e);
			throw e;
		}finally{
			super.flush();
		}
		return count;
	}
	
	public int delete(int id) throws Exception{
		int count = 0;
		try{
			logger.debug("delete >> id:"+id);
			
			super.connect();
			
			if(conn != null){
				StringBuffer buffer = new StringBuffer("");
				buffer.append(" update Task ");
				buffer.append("  set active = ? ");
				buffer.append(" where id = ? ");
				sql = buffer.toString();
				
				pstm = conn.prepareStatement(sql);
				
				byte i = 1;
				pstm.setByte(i++, Constants.Active.NO);
				pstm.setInt(i++, id);
				
				count = pstm.executeUpdate();
			}
		}catch(Exception e){
			logger.error("delete error:",e);
			throw e;
		}finally{
			super.flush();
		}
		return count;
	}
}
