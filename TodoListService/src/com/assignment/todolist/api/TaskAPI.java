package com.assignment.todolist.api;


import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.assignment.todolist.helper.ConvertUtil;
import com.assignment.todolist.logic.TaskManager;
import com.assignment.todolist.model.TaskStatus;
import com.assignment.todolist.model.Task;

@Path("/tasks")
public class TaskAPI {
	TaskManager manager = new TaskManager();
	
	@GET
	@Path("/{taskId}")
	@Produces("application/json")
	public Response get(@PathParam("taskId") String id) {
		try{
			int taskId = ConvertUtil.toInt(id);
			Task task = manager.get(taskId);
			if(task != null && task.getId() != 0){
				return Response.status(Response.Status.OK).entity(task).build();
			}else{
				return Response.status(Response.Status.NOT_FOUND).entity("Task doesn't exist").build();
			}			
		}catch(Exception e){
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
	}
	
	@GET
	@Path("")
	@Produces("application/json")
	public Response getAll() {
		try{
			List<Task> taskList = manager.getAll();
			return Response.status(Response.Status.OK).entity(taskList).build();
		}catch(Exception e){
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
	}
	
	@POST
	@Path("")
	@Consumes("application/json")
	public Response add(Task task) {
		try{
			int id = manager.add(task);
			return Response.status(Response.Status.CREATED).entity(String.format("Task %s Created", id)).build();
		}catch(Exception e){
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
	}
	
	@PUT
	@Path("/{taskId}")
	@Consumes("application/json")
	public Response edit(@PathParam("taskId") String id, Task task) {
		try{
			int taskId = ConvertUtil.toInt(id);
			task.setId(taskId);
			int count = manager.edit(task);			
			if(count != 0){
				return Response.status(Response.Status.CREATED).entity(String.format("Task %s Edited", id)).build();
			}else{
				return Response.status(Response.Status.NOT_FOUND).entity("Task doesn't exist").build();
			}
		}catch(Exception e){
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
	}
	
	@PUT
	@Path("{taskId}/status")
	@Consumes("application/json")
	public Response setStatus(@PathParam("taskId") String id, TaskStatus status) {
		try{
			int taskId = ConvertUtil.toInt(id);
			int count = manager.setStatus(taskId, status.getStatusId());
			if(count != 0){
				return Response.status(Response.Status.CREATED).entity(String.format("Task id %s set status to %s", id, status.getStatusId())).build();
			}else{
				return Response.status(Response.Status.NOT_FOUND).entity("Task doesn't exist").build();
			}			
		}catch(Exception e){
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
	}
	
	@DELETE
	@Path("/{taskId}")
	@Consumes("application/json")
	public Response delete(@PathParam("taskId") String id) {
		try{
			int taskId = ConvertUtil.toInt(id);
			int count = manager.delete(taskId);			
			if(count != 0){
				return Response.status(Response.Status.NO_CONTENT).build();
			}else{
				return Response.status(Response.Status.NOT_FOUND).entity("Task doesn't exist").build();
			}
		}catch(Exception e){
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
	}
}
