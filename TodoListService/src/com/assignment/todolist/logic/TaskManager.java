package com.assignment.todolist.logic;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.assignment.todolist.dao.TaskDAO;
import com.assignment.todolist.model.Task;

public class TaskManager {
	
	private static final Logger logger = LogManager.getLogger(TaskManager.class);
	
	TaskDAO dao = new TaskDAO();
	
	public Task get(int id) throws Exception{
		Task task = null;
		try{
			task = dao.get(id);
		}catch(Exception e){
			logger.error("get error:",e);
			throw e;
		}
		return task;
	}
	
	public List<Task> getAll() throws Exception{
		List<Task> taskList = null;
		try{
			taskList = dao.getAll();
		}catch(Exception e){
			logger.error("getAll error:",e);
			throw e;
		}
		return taskList;
	}
	
	public int add(Task task) throws Exception{
		int id = 0;
		try{
			id = dao.add(task);
		}catch(Exception e){
			logger.error("add error:",e);
			throw e;
		}
		return id;
	}
	
	public int edit(Task task) throws Exception{
		int count = 0;
		try{
			count = dao.edit(task);
		}catch(Exception e){
			logger.error("edit error:",e);
			throw e;
		}
		return count;
	}
	
	public int setStatus(int id, int statusId) throws Exception{
		int count = 0;
		try{
			count = dao.setStatus(id, statusId);
		}catch(Exception e){
			logger.error("setStatus error:",e);
			throw e;
		}
		return count;
	}
	
	public int delete(int id) throws Exception{
		int count = 0;
		try{
			count = dao.delete(id);
		}catch(Exception e){
			logger.error("delete error:",e);
			throw e;
		}
		return count;
	}
}
