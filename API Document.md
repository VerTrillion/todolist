# API Document

## Get All Tasks
-----------------------
  Returns All Tasks in List.

* **URL**  

    /tasks  

* **Method:**  

    `GET`  

* **URL Params**  

    None  

* **Data Params**  

    None  

* **Success Response:**  

    * **Code:** 200 OK  
    **Content:** ```[{"id":3,"subject":"Eat Apple","desc":"Green Apple","statusId":1},{"id":4,"subject":"Listen to music","desc":"New Divide","statusId":0},{"id":5,"subject":"Swim in the lake","desc":"In National Park","statusId":1}]```  

* **Error Response:**  

    * **Code:** 500 INTERNAL ERROR  
    **Content:** `Exception Message`  

* **Sample Call:**  

        $.ajax({
          url: "/tasks",
          dataType: "json",
          method: "GET",
          success: function(data) {
            console.log(data);
          }
        });

## Get Task
-----------------------
  Returns Task in List.

* **URL**  

    /tasks/:id  

* **Method:**  

    `GET`  

* **URL Params**  

    **Required:**  
    `id=[integer]`  

* **Data Params**  

    None  

* **Success Response:**  

    * **Code:** 200 OK  
    **Content:** ```{"id":3,"subject":"Eat Apple","desc":"Green Apple","statusId":1}```  
 
* **Error Response:**  

    * **Code:** 404 NOT FOUND  
    **Content:** `"Task doesn't exist"`  

    * **Code:** 500 INTERNAL ERROR  
    **Content:** `[Exception Message]`  

* **Sample Call:**  

        $.ajax({
          url: "/tasks/3",
          dataType: "json",
          method: "GET",
          success: function(data) {
            console.log(data);
          }
        });


## Add Task
-----------------------
  Add Task into List.

* **URL**  

    /tasks  

* **Method:**  

    `POST`  

* **URL Params**  

    None  

* **Data Params**  

    ```{"subject":[string], "desc":[string]}```  

* **Success Response:**  

    * **Code:** 201 CREATED  
    **Content:** ```"Task [taskId] Created"```  
 
* **Error Response:**  

    * **Code:** 404 NOT FOUND  
    **Content:** `"Task doesn't exist"`  
    
    * **Code:** 500 INTERNAL ERROR  
    **Content:** `Exception Message`  

* **Sample Call:**  

        $.ajax({
          url: "/tasks",
          method: "POST",
          dataType: "json",
          data: "{
            "subject":"Eat Apple",
            "desc":"Green Apple"
          }",
          success: function(data) {
            console.log(data);
          }
        });

## Edit Task
-----------------------
  Edit existing Task in List.

* **URL**  

    /tasks/:id  

* **Method:**  

    `PUT`  

* **URL Params**  

    **Required:**  
    `id=[integer]`  

* **Data Params**  

    ```{"subject":[string], "desc":[string]}```  

* **Success Response:**  

    * **Code:** 201 CREATED  
    **Content:** ```"Task [taskId] Edited"```  
 
* **Error Response:**  

    * **Code:** 404 NOT FOUND  
    **Content:** `"Task doesn't exist"`  
    
    * **Code:** 500 INTERNAL ERROR  
    **Content:** `Exception Message`  

* **Sample Call:**  

        $.ajax({
          url: "/tasks/3",
          method: "PUT",
          dataType: "json",
          data: "{
            "subject":"Eat Apple",
            "desc":"Green Apple"
          }",
          success: function(data) {
            console.log(data);
          }
        });

## Edit Task status
-----------------------
  Edit Task status in List.

* **URL**  

    /tasks/:id/status  

* **Method:**  

    `PUT`  

* **URL Params**  

    **Required:**  
    `id=[integer]`  

* **Data Params**  

    ```{"statusId":[integer], "statusName":[string]}```  

* **Success Response:**  

    * **Code:** 201 CREATED  
    **Content:** ```"Task id [taskId] set status to [statusId]"```  
 
* **Error Response:**  

    * **Code:** 404 NOT FOUND  
    **Content:** `"Task doesn't exist"`  
    
    * **Code:** 500 INTERNAL ERROR  
    **Content:** `Exception Message`  

* **Sample Call:**  

        $.ajax({
          url: "/tasks/3/status",
          method: "PUT",
          dataType: "json",
          data: "{
            "statusId":1,
            "statusName":"Done"
          }",
          success: function(data) {
            console.log(data);
          }
        });

## Delete Task
-----------------------
  Delete existing Task from List.

* **URL**  

    /tasks/:id  

* **Method:**  

    `DELETE`  

* **URL Params**  

    **Required:**  
    `id=[integer]`  

* **Data Params**  

    NONE  

* **Success Response:**  

    * **Code:** 204 NO CONTENT  
 
* **Error Response:**  

    * **Code:** 404 NOT FOUND  
    **Content:** `"Task doesn't exist"`  
    
    * **Code:** 500 INTERNAL ERROR  
    **Content:** `Exception Message`  

* **Sample Call:**  

        $.ajax({
          url: "/tasks/3",
          method: "DELETE",
          dataType: "json",
          success: function(data) {
            console.log(data);
          }
        });